set(CMAKE_C_COMPILER   clang)
set(CMAKE_CXX_COMPILER clang++)

cmake_minimum_required(VERSION 2.8)
include(GNUInstallDirs)

project(libcxx CXX C)
set(LIBCXX_VERSION 1)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -fpic -fstrict-aliasing -Wall -Wextra -pedantic -Wunused -Wshadow -Wconversion -Wstrict-overflow=4 -Wpadded -Wmissing-prototypes -Wmismatched-tags -Wshorten-64-to-32 -Wnewline-eof")
set(LIBCXX_LINK_FLAGS "-Wl,--as-needed -Wl,--no-undefined -nodefaultlibs")

include_directories(
  ${CMAKE_SOURCE_DIR}/libcxxabi/include
  ${CMAKE_SOURCE_DIR}/libcxx/include
)

# ----------------------------------------------------------------------------
# libc++abi
set(CXXABI_SOVERSION "${LIBCXX_VERSION}")
set(CXXABI_VERSION   "${CXXABI_SOVERSION}.0")
file(GLOB CXXABI_SOURCES ${CMAKE_SOURCE_DIR}/libcxxabi/src/*.cpp)

add_library(c++abi STATIC
  ${CXXABI_SOURCES}
)
target_link_libraries(c++abi pthread c -Wl,-Bstatic unwind -Wl,-Bdynamic)
set_target_properties(c++abi PROPERTIES
  VERSION       "${CXXABI_VERSION}"
  SOVERSION     "${CXXABI_SOVERSION}"
  SONAME        "libc++abi${CXXABI_SOVERSION}"
  LINK_FLAGS    "${LIBCXX_LINK_FLAGS}"
)

# ----------------------------------------------------------------------------
# libc++
set(CXX_SOVERSION "${LIBCXX_VERSION}")
set(CXX_VERSION   "${CXX_SOVERSION}.0")
file(GLOB CXX_SOURCES ${CMAKE_SOURCE_DIR}/libcxx/src/*.cpp)

add_library(c++ SHARED
  ${CXX_SOURCES}
)
target_link_libraries(c++ c++abi pthread c rt)
set_target_properties(c++ PROPERTIES
  VERSION       "${CXX_VERSION}"
  SOVERSION     "${CXX_SOVERSION}"
  SONAME        "libc++${CXX_SOVERSION}"
  LINK_FLAGS    "${LIBCXX_LINK_FLAGS}"
)

# ----------------------------------------------------------------------------
# installation
install(TARGETS c++
  LIBRARY DESTINATION "${CMAKE_INSTALL_LIBDIR}/$ENV{DEB_HOST_MULTIARCH}"
)
install(DIRECTORY ${CMAKE_SOURCE_DIR}/libcxxabi/include/
  DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/c++/v${LIBCXX_VERSION}
)
install(DIRECTORY ${CMAKE_SOURCE_DIR}/libcxx/include/
  DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/c++/v${LIBCXX_VERSION}
)

if(DEFINED LIBCXX_ADD_TESTS)
# ----------------------------------------------------------------------------
# testing
file(GLOB_RECURSE CXXABI_TESTS ${CMAKE_SOURCE_DIR}/libcxxabi/test/*.cpp)
file(GLOB_RECURSE CXX_TESTS ${CMAKE_SOURCE_DIR}/libcxx/test/*.cpp)

set(CXXABI_TEST_RESULTS ${CMAKE_BINARY_DIR}/c++abi-test-results)
set(CXX_TEST_RESULTS ${CMAKE_BINARY_DIR}/c++-test-results)

add_custom_target(test)
add_custom_target(test-c++
  COMMAND cat ${CXX_TEST_RESULTS}
  COMMAND echo tests failed: `grep    failed ${CXX_TEST_RESULTS} | wc -l`/`cat ${CXX_TEST_RESULTS} | wc -l`
  COMMAND echo tests passed: `grep -v failed ${CXX_TEST_RESULTS} | wc -l`/`cat ${CXX_TEST_RESULTS} | wc -l`
  COMMAND rm ${CXX_TEST_RESULTS}
)
add_custom_target(test-c++abi
  COMMAND cat ${CXXABI_TEST_RESULTS}
  COMMAND echo tests failed: `grep    failed ${CXXABI_TEST_RESULTS} | wc -l`/`cat ${CXXABI_TEST_RESULTS} | wc -l`
  COMMAND echo tests passed: `grep -v failed ${CXXABI_TEST_RESULTS} | wc -l`/`cat ${CXXABI_TEST_RESULTS} | wc -l`
  COMMAND rm ${CXXABI_TEST_RESULTS}
)
add_dependencies(test test-c++ test-c++abi)
add_dependencies(test-c++ c++)
add_dependencies(test-c++abi c++abi)

set(LIBCXX_TEST_INCLUDE_DIRS -I${CMAKE_SOURCE_DIR}/libcxxabi/include -I${CMAKE_SOURCE_DIR}/libcxx/include)
set(LIBCXX_TEST_LIBRARY_DIRS -L${CMAKE_BINARY_DIR})
set(LIBCXX_TEST_COMPILE      "${CMAKE_CXX_COMPILER} ${CMAKE_CXX_FLAGS} -stdlib=libc++ ${LIBCXX_TEST_INCLUDE_DIRS} ${LIBCXX_TEST_LIBRARY_DIRS}")
string(REPLACE " " ";" LIBCXX_TEST_COMPILE "${LIBCXX_TEST_COMPILE}")

macro(libcxx_add_test LIBCXX_TEST_SUITE LIBCXX_TEST_SOURCE)
  string(REPLACE "${CMAKE_SOURCE_DIR}/" "" LIBCXX_TEST_NAME "${LIBCXX_TEST_SOURCE}")
  string(REPLACE "/" "-" LIBCXX_TEST_NAME "${LIBCXX_TEST_NAME}")
  string(REPLACE "." "-" LIBCXX_TEST_NAME "${LIBCXX_TEST_NAME}")
  string(REPLACE "-cpp" "" LIBCXX_TEST_NAME "${LIBCXX_TEST_NAME}")
  string(REGEX MATCH "(fail|pass)$" LIBCXX_TEST_TYPE "${LIBCXX_TEST_NAME}")
  string(REGEX MATCH "=" LIBCXX_TEST_CONTAINS_EQUAL "${LIBCXX_TEST_NAME}")
  set(LIBCXX_TEST_RESULTS ${CMAKE_BINARY_DIR}/${LIBCXX_TEST_SUITE}-test-results)

  set(LIBCXX_TEST_OUTPUT ${CMAKE_BINARY_DIR}/${LIBCXX_TEST_NAME})

  if("${LIBCXX_TEST_CONTAINS_EQUAL}" STREQUAL "=")
    message(STATUS "skip test ${LIBCXX_TEST_NAME}")
  else()
    message(STATUS "add test ${LIBCXX_TEST_NAME}")
    if("${LIBCXX_TEST_TYPE}" STREQUAL "fail")
      add_custom_target(${LIBCXX_TEST_NAME}
        COMMAND ${LIBCXX_TEST_COMPILE} ${LIBCXX_TEST_SOURCE} -o ${LIBCXX_TEST_OUTPUT} 2>/dev/null && echo "failed to fail:  ${LIBCXX_TEST_NAME}" >> ${LIBCXX_TEST_RESULTS} || echo "pass:            ${LIBCXX_TEST_NAME}" >> ${LIBCXX_TEST_RESULTS}
        SOURCES ${LIBCXX_TEST_SOURCE}
      )
    else()
      add_custom_target(${LIBCXX_TEST_NAME}
        COMMAND ${LIBCXX_TEST_COMPILE} ${LIBCXX_TEST_SOURCE} -o ${LIBCXX_TEST_OUTPUT} 2>/dev/null && ${LIBCXX_TEST_OUTPUT} && echo "pass:            ${LIBCXX_TEST_NAME}" >> ${LIBCXX_TEST_RESULTS} || echo "failed to run:   ${LIBCXX_TEST_NAME}" >> ${LIBCXX_TEST_RESULTS} || echo "failed to build: ${LIBCXX_TEST_NAME}" >> ${LIBCXX_TEST_RESULTS}
        SOURCES ${LIBCXX_TEST_SOURCE}
      )
    endif()
    add_dependencies(test-${LIBCXX_TEST_SUITE} ${LIBCXX_TEST_NAME})
    add_dependencies(${LIBCXX_TEST_NAME} ${LIBCXX_TEST_SUITE})
  endif()
endmacro()

foreach(CXXABI_TEST ${CXXABI_TESTS})
  libcxx_add_test(c++abi ${CXXABI_TEST})
endforeach()

foreach(CXX_TEST ${CXX_TESTS})
  libcxx_add_test(c++ ${CXX_TEST})
endforeach()
endif()
